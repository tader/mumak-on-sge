#!/bin/bash

mumak_generate_config() {
    # Populate CONF for this job
    if [ ! -d "$WORK_HADOOP_CONF" ]; then mkdir -p "$WORK_HADOOP_CONF"; fi
    if [ ! -d "$WORK_MUMAK_CONF" ]; then mkdir -p "$WORK_MUMAK_CONF"; fi

	cp "$HADOOP_DEFAULT_CONF/"* "$WORK_HADOOP_CONF/"
	cp "$MUMAK_DEFAULT_CONF/"*  "$WORK_MUMAK_CONF/"
	cp "$ETC/"*.xml             "$WORK_MUMAK_CONF/"

	# Create a new hadoop-site.xml for this job in the grid.
	cp "$MUMAK_CONF_TEMPLATE" "$WORK_MUMAK_CONF/mumak.xml"

    # Escape for sed replacement
    # See: http://stackoverflow.com/questions/407523/escape-a-string-for-sed-search-pattern

    _WORK_HADOOP_CONF=$( echo "${WORK_HADOOP_CONF}"  |sed -e 's/\(\/\|\\\|&\)/\\&/g')
    _WORK_MUMAK_CONF=$(  echo "${WORK_MUMAK_CONF}"   |sed -e 's/\(\/\|\\\|&\)/\\&/g')
    _MUMAK_LOG_DIR=$(    echo "${MUMAK_LOG_DIR}"     |sed -e 's/\(\/\|\\\|&\)/\\&/g')
    _SUBMISSION_POLICY=$(echo "${SUBMISSION_POLICY}" |sed -e 's/\(\/\|\\\|&\)/\\&/g')

    sed -i "s/%%HADOOP_CONF%%/$_WORK_HADOOP_CONF/g"        "$WORK_MUMAK_CONF/mumak.xml"
    sed -i "s/%%MUMAK_CONF%%/$_WORK_MUMAK_CONF/g"          "$WORK_MUMAK_CONF/mumak.xml"
    sed -i "s/%%MUMAK_LOG_DIR%%/$_MUMAK_LOG_DIR/g"         "$WORK_MUMAK_CONF/mumak.xml"
    sed -i "s/%%SUBMISSION_POLICY%%/$_SUBMISSION_POLICY/g" "$WORK_MUMAK_CONF/mumak.xml"

	cp "$WORK_MUMAK_CONF/mumak.xml" "$WORK_HADOOP_CONF/mapred-site.xml"
}

mumak_run() {
    if [ ! -d "$MUMAK_LOG_DIR"  ]; then mkdir -p "$MUMAK_LOG_DIR";  fi
    if [ ! -d "$HADOOP_LOG_DIR" ]; then mkdir -p "$HADOOP_LOG_DIR"; fi
    if [ ! -d "$HADOOP_TMP_DIR" ]; then mkdir -p "$HADOOP_TMP_DIR"; fi

    HADOOP_OPTS="$HADOOP_OPTS -Dmumak.log.dir=$MUMAK_LOG_DIR"
    HADOOP_OPTS="$HADOOP_OPTS -Dhadoop.log.dir=$HADOOP_LOG_DIR"
    HADOOP_OPTS="$HADOOP_OPTS -Dhadoop.tmp.dir=$HADOOP_LOG_DIR/tmp"
    HADOOP_OPTS="$HADOOP_OPTS -Djava.library.path=$HADOOP_JAVA_LIBRARY_PATH"
    HADOOP_OPTS="$HADOOP_OPTS -Dhadoop.policy.file=$HADOOP_POLICYFILE"

    CMD="/usr/bin/time --output \"$WORK_DIR/mumak-time.txt\" -- \"$JAVA\" -enableassertions $JAVA_HEAP_MAX $HADOOP_OPTS -classpath \"$MUMAK_CLASSPATH\" org.apache.hadoop.mapred.SimulatorEngine -conf=${WORK_MUMAK_CONF}/mumak.xml $MUMAK_WORKLOAD_FILE $MUMAK_TOPOLOGY_FILE"
    echo "$CMD"
    eval "$CMD"
}

mumak_report() {
    trap - INT TERM EXIT

    (

        echo "START TIME:          $START_TIME"
        echo "WORKLOAD GENERATED:  $GENERATION_DONE_TIME"
        echo "END TIME:            `date`"
        echo
        echo "MUMAK TIME:"
        cat "$WORK_DIR/mumak-time.txt"
        echo
        echo "WORKLOAD_MODEL:      $WORKLOAD_MODEL"
        echo "WORKLOAD_SEED:       $WORKLOAD_SEED"
        echo "WORKLOAD_LOAD:       $WORKLOAD_LOAD"
        echo "WORKLOAD_START:      $WORKLOAD_START"
        echo "WORKLOAD_RUNTIME:    $WORKLOAD_RUNTIME"
        echo "TOPOLOGY_FILE:       $TOPOLOGY_FILE"
        echo "MUMAK_WORKLOAD_FILE: $MUMAK_WORKLOAD_FILE"
        echo "MUMAK_TOPOLOGY_FILE: $MUMAK_TOPOLOGY_FILE"
        echo
        echo "Mumak Workload MD5:"
        md5sum "$MUMAK_WORKLOAD_FILE"
        echo
        echo "Workload Model:"
        cat "$WORKLOAD_MODEL"

    ) >"$MUMAK_REPORT"

    cp  -v    "$MUMAK_REPORT"      "$WORK_DIR/"
    cp  -v    "$WORKLOAD_MODEL"    "$WORK_DIR/"
    cp  -v    "$TOPOLOGY_FILE"     "$WORK_DIR/"
    tar -cvzf "$MUMAK_REPORT_LOG"  "$WORK_DIR"
    tar -tvzf "$MUMAK_REPORT_LOG" >"$MUMAK_REPORT_LOG.contents.txt" 

    rm  -rvf  "$WORK_DIR"
    rm  -rvf  /tmp/Jetty*

    exit 0
}
